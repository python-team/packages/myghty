from paste.script.templates import Template

class RoutesTemplate(Template):
    _template_dir = '../paster_templates/routes'
    summary = 'Routes Template'
    
class SimpleTemplate(Template):
    _template_dir = '../paster_templates/simple'
    summary = 'Simple Template'

class MCTemplate(Template):
    _template_dir = '../paster_templates/modulecomponents'
    summary = 'Module Component Template'
    
