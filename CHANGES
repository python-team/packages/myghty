1.1
- added unicode support ([#9])
- changed handling of non-string arguments to m.write and m.apply_escapes.
  all object, except None, get coerced to a string.  (None get converted
  to the empty string.)
- the out_buffer argument of m.create_subrequest now works ([#12])

1.0.2
- disable_wrapping argument to request will allow a full request
to take place without calling any inherited components
(inheritance relationship still exists though)
- fix to do_remove() on MemoryNamespaceManager

1.0.1
- fixed lexer bug reporting error with closing tag missing
- html exception reporting html-escapes error message 
- session object takes "cookie_expires" - True (end of session),
False (never), datetime, or timedelta
- fixed closure-with-content bug

1.0 
- lots more docstrings, plus a generated pydoc section 
in the docs.  still many more docstrings to go.
- m.abort takes optional "reason" parameter along with
return code
- License changed to MIT License
- WSGIHandler no longer url un-escapes PATH_INFO
- MODULE:modname:callable syntax can also be called as:
@modname:callable.  
- oops again, its "__all__", not "__ALL__" !  make sure all
your code that tries to "from myghty import *" still works.
- added more docs for configuration, resolution
- added helper functions for writing custom resolvers: 'cache_args'
to ModuleComponentSource, default 'file modification time' for 
FileComponentSource
- added module_root_adjust in place of 'adjust' for ResolvePathModule,
component_root_adjust in place of 'adjust' for FileResolver.
- ResolvePathModule arguments are also global config parameters
- argument to path_translate/PathTranslate() can be a callable instead
of a list

0.99a
- added 'ZBlog' demo, a bigger MVC framework which interfaces
to SQLAlchemy as well as an enhanced Myghtyjax (mjax docs
forthcoming hopefully)
- component-calls-with-content can send named arguments to 
m.content(), function can get at them via m.content_args[<name>]
- fixed InterpreterName not working with ApacheHandler bug
- added "deletion check" to synchronizer, dbmcontainer/cache to 
gracefully re-create the directory if not present, allows 
straight deletion of cache/lock directories to reset an apps cache
without restart
- added last modified time to modulecomponents so that they support          
m.cache_self() and other functions
- corrections to module importer to allow accurate "last modified"
times; new unit tests contributed by dairiki
- fixed AbortRequest exceptions to subclass Error so that they
properly return their text representation
- HTTPServerHandler runner wasnt sending error headers properly,
should be fixed...
- added 'require_publish' option, picked up by ResolvePathModule,
to enable 'positive publish' checking on functions and methods.
looks for 'publish' attribute on the callable - might make a 
decorator function for this.
- added get_method on FunctionComponent.  so when you have an
implicit module component that is a method on an object,
you can call get_method to get another FunctionComponent for 
another method on the object.
- added "path_stringtokens" and "path_moduletokens" arguments
to ResolvePathModule.  it will use these two lists as "default"
tokens to search for if none are found otherwise.
- added __delitem__ to util.InheritedDict, allows "del" to be
called on request.attributes and component.attributes to remove
locally set attributes
- bugfix to objgen.py, involving indentation calculation of
lines with <something>: #<otherthing>.  Which could be:
	if x==5:  # check for x
or could be:
	m.write("font-color: #F0F0F0");
oopsie !
- escapes in substitutions should be compiled in order now,
so they execute in the order they were placed in the 
substitution.
- exception HTML page correctly formats spaces in quoted
code sections, courtesy Deron Meranda
- %closure funcs can now be called as component-calls-with-content

0.99
- added routes resolver, uses Routes for rails-like resolution
- added built-in Paste templates for creating sample installations
- switched to setuptools to allow Paste entry points plus dependency
installation
- added display of the wrapped exception name when reporting 
exceptions
- added handling of .pyo files along with .pyc files when python
is run with -O flag
- changed behavior of resolution Group to maintain url adjustments
when transferring from its internal chain of rules to the external
chain
- fixed import bug in Container which was not allowing "type='dbm'"
- added "silent=True" option to resolver NotFound rule, to allow
"fall-through" into another framework without dumping tons
of errors into the log.  propigates into 
TopLevelNotFound and is picked up by HTTPHandler.

0.98c
- <%requestlocal> and <%threadlocal> do not get top-level component arguments
in their generated method signatures, so that they can be called off of a 
<%method> call as well as a top-level component
- <%filter> section also gets m, ARGS, plus global arguments in its generated
method signature so that they are available, are supplied at runtime
- small fix to "magic number" recompiling to work properly with the current
file importer, as the filter change has bumped up the "compile id" up to 4

0.98b
- fixed non-standard global variables, such as 'r' and
user-defined globals, so that they appear by name in 
<%threadlocal> and <%requestlocal> sections
- fixed component dynamic args, which were not working before
- fixed component attributes so they inherit an arbitrary
number of levels towards the parent, before they were stopping after 
one level
- session fully implements dict interface
- added memcached extension module for container, works with
cache and session
- beefed up use_static_source.  all file-stats and reloads
are disabled if use_static_source is enabled.  templates will not
even be re-compiled across restarts unless their corresponding .py 
files are deleted from the cache directory, although module components
get loaded on each server startup.

0.98a
- empty component-calls-with-content bug repaired
- fixed global_args so that request-level global_args are definitely
unique vs. the interpreter-wide global_args, and also against
other requests.
- global_args default to None for each global, so you can declare 
just allow_globals and thats all.
- fixed small import thing in interp that could happen in certain configs
- added __delitem__ to cache so that "del" is supported for cache keys

0.98
- simplified HTTP interface to use get_handler() and 
handle() functions; WSGI gets application(environ, start_response)
callable as well.
- custom global_args can now be safely specified at both the 
interpreter level and the request level simultaneously.
- corrected ResolvePathModule to not resolve into modules within modules
- docs docs docs

0.98alpha2
- fixed reentrant-related bug in memory caching with m.cache_self()
- reorganization of file-based module loading.  file-based modules are 
immediately removed from sys.modules thereby eliminating any need for
management upon component deletion.  'delete_modules' option removed.
- added <%closure> tag - defines a function in the local scope which is 
callable like a regular component. supports <%args>, <%init>, <%cleanup>
as well.
- on-change component reloading added for compiled-in-memory components 
- fixed the backslash operator for suppressing newlines
- SimpleHTTPHandler was url_unescape()ing path_info, removed that. 
- docs - slowly but surely

0.98alpha
- added implicit module components, module components that are
created directly from a plain function, callable object, or object
method.
- old-style ModuleComponents, called 'explicit module components',
no longer require you declare the args in the do_component_init
method; it will look them up automatically if you dont supply
- added module_root/ResolvePathModule which resovles all types of 
module components via path traversal, in the style of mod_python publisher.
- overhauled internal component args handling and added new 
<%args scope="dynamic"> option.
- added options to ResolveModule to support implicit module components
- changed shopping cart example to use implicit module components,
added commandline runner
- redid escapes module to use cgi.escape, urllib.quote_plus with a 
unicode fix-it step
- added 'x' escape flag plus xml_escape to escapes
- added myghty.escapes.html_entities_escape
- HTTPHandler sets content type text/html before reporting errors
- removed unused "feature" that affected urls with : in them
- overhaul of docs for config, module components, resolution in progress

0.97b
- fixes to module memory management so it works with in-memory components
- fix to a potential race condition in module memory management
- some cleanup of examples, slight documentation tweaks

0.97a
- ApacheHandler imports myghty.resolver symbols to do resolver_strategy
in httpd.conf
- fixed interp and request handling of MODULE:module:class -style component 
calls
- added formvisitor example
- added raise_error config param to directly raise exceptions with no handling
- deprecated docroot param in standalone server and integrated plain-
document serving with Interpreter directives for more flexible setups
- took the plunge; all source code tabs converted to spaces
- added a rudimentary not-yet-documented module components arguments thing
to the resolver

0.97
- changed error_handler to accept regular m and optional r argument
- added error handling for the error_handler itself
- request object has resolver instance for lead component attached to it
- fixes to WSGIHandler to help demo site run in Paste
- demo server uses just one resolver setup  

0.97alpha3
- soft send_redirect will parse out query arguments right of the ? and add 
them to the request_args for the subexec
- fixed dir_name bug in resolver where dir_name was blank for a uri with no slashes
- added check in component to insure it doesnt locate its parent recursively
- added test cases for resolver, component

0.97alpha2
- new resolver implementation, is totally rule-driven and allows total control
over the resolution of all uris, including path translation, module components,
file components, dhandlers, autohandlers, caching of uris.  allows custom 
rule chains and subchains to be constructed which can be triggered via 
regular expression or contextual commands
- reworked demo runner to illustrate resolver rules
- added Ajax toolkit example
- rolled out unittest-based test framework, added unit tests for resolver
- fixed glitch in syntax highlighter
- added error_handler argument to requestimpl to allow custom error handling
- added FileContainer implementation, replaces DBMContainer as default for Session
- added new interpreter debug option "debug_elements", fine-grained control
over debug output
- seldom-needed stability enhancements to exception formatting (i.e., when
template processing itself is broken for some strange reason, it wont crash)

0.97alpha
- code_cache_size is now measured in bytes.  the default is the affable
16777216.
- when a FileComponent is garbage collected, because it has fallen
out of the code_cache, its __del__ method carefully and synchronously 
checks that it is definitely out of use, then removes its originating
module from sys.modules, thereby freeing up the memory it used.
- to test this sensitive operation, test/MemUsage.py tester added 
to illustrate memory usage when the code cache needs to expire
many components, all of which might be called up again.
- components have the "persistent" flag to specify their module
definitely should/should not be deleted from sys.modules, interpreter has the
"delete_modules" option (defaults to True) to switch the entire feature 
off
- added namespace_class parameter to session, for providing custom
NamespaceManager implementations
- added encoding flag, which allows a component to declare its character
encoding.  translates directly in to the generated python module's 
encoding comment.
- fixed glitch where demo server didnt browse paths correctly on windows
platforms
- fixed glitch with exception string formatting for exceptions called before
interpreter execute


0.96f
- fix to session.py so that secret-encoded cookies dont get trampled by 
other cookies
- flags added to containment/sync APIs to not encode filenames
- session uses the flag so that session files correspond directly to IDs
- auto-id generation added to MemoryComponentSource, id argument optional 
to interpreter.make_component() again
- some session parameters added to shopping cart example for illustration

0.96e
- fix to HTTPHandler clone() method to allow configuration args 
use_session, global_args, output_errors, log_errors to propigate
into subrequests


0.96d
- interpreter_name param added (MyghtyInterpreterName in mod_python conf), 
allows HTTPHandler to keep separate instances of interpreter based on name
- added path_translate configuration parameter, a list of regexp 
search and replaces that will be performed on paths before 
resolving, as well as m.interpreter.resolver.translate_path() for 
programmatic access
- refactoring of HTTPHandler implementations to be more consistent,
correctly sends content-type header for error responses
- simplifications to demo server making use of new features

0.96c
- setup.py script had a path error in it, wasnt installing HTTP 
modules

0.96b
- request_path was broken in non-dhandler classes, was fixed,
but also it is now the original request_path in all cases.
- added dhandler_path, which is the request_path adjusted
to the currently serving dhandler.  dhandler_arg is still there which
references the same thing.
- added root_request_path, root_request request members to reference
the root request.  parent_request member documented.
- WSGIHandler working in a rudimentary fashion.  Tested against the PEAK
WSGI reference server so far, available in the CVS tree of 
http://peak.telecommunity.com/.
- interp.make_component() requires a component id, as python interpreter 
needs unique identifiers when creating in-memory components
- added python syntax highlighting to documentation and examples

0.96a
- fixed issue with source browser
0.96
- session module added, adds session support to all HTTP
environments, independent of mod_python
- module components - a third form of component which
are pure Python modules, run similarly to a servlet with
full myghty namespace and request support
- most get_XXX methods of Request and Component have been
changed to straight attribute access, to make the interface
more "Pythonic" and better-performing.  Old methods mostly
remain for compatibility.
- added 'file' data member to Component.  returns 
the filesystem path of the component, if one exists.
- an attributes dictionary has been added to Interpreter, which
is set via configuration parameters or programmatically.  
This same hierarchical attribute interface has been 
added to Request and Component, replacing the notes() and 
attributes() methods (old accessors still exist).
- <%args scope="request"> will refer to the request arguments
sent to the topmost request, so that the main request and
subrequests all reference the same original request.  
<%args scope="subrequest"> added to allow access to local subrequest
arguments.
- indent calculation for % lines overhauled.  handles 
compound statements which can contain any mix of single-
line or multi-line statement suites
- shopping cart example added, has comprehensive usage of
module components, session, introduces form handling and
state machine paradigms
- "new style classes" largely implemented, many methods of request
and component are now properties (but the methods still exist for 
backwards compatibility)
- cache arguments for the get_cache(), cache.get() methods can be 
specified with or without "cache_" prefix.  session arguments
also are accessed with our without "session_" prefix.  When 
specified as configuration parameters, the prefixes are required.
- http handlers refactored to extend from a common base HTTPHandler.
handlers have all been moved into their own "http/" directory, but 
are still importable via the "myghty" package.
- WSGI handler not completed but partially implemented.
- standalone HTTP handler building off of python BaseHTTPHandler
package added.
- demo/docs server added on top of standalone HTTP handler, 
serves documentation, all examples, full distribution 
source code browser
- ApacheHandler catches IOErrors during exception writing to
more cleanly handle dropped client connections
- implemented remove() method in NamespaceManager objects,
cleans up dbm files or memory
- synchronizer cleans out lockfiles when locks fall out
of use
- more rewriting of documentation, particularly the Installation,
Configuration, Inheritance, Request, Components sections.  
Configuration parameters, data members and methods are cross-referenced
via hyperlinks.  New sections include Session, Module Components.

0.95c
- fixed m.decline(), had a python error
- fixed m.dhandler_arg() behavior to work with root dhandlers
- fixed overall behavior of decline()/dhandler_arg() to work...
correctly !  I think ! 
- added m.log() method to request, RequestImpl
- added "#" comment lines
- documentation gen system genericized

0.95b
- 'self' variable is available in components
- methods of file-based components cleaned up, available in 
subcomponents as well, also added to documentation.
- methods can be programmatically called from the top level Interpreter
- return value of a component returned by Interpreter.execute()
- compiler upgrades will automatically recompile components with incompatible
magic numbers, instead of raising an error

0.95a
- m.abort() takes optional HTTP return code argument and returns 
code via HTTP, else silently aborts
- added fetch_all(), caller(), callers(), call_stack(), caller_args()
to request
- added 'raise_error=True' option to m.fetch_component() 
- documentation edits, new sections, going going going....

0.95
- subcomponents and methods are allowed %cleanup sections 
- "scope" added to %python tag, replacing %init, %cleanup, %global, %threadlocal,
%requestlocal tags and their synonyms (though those tags are still
available) 
- "scope" added to %args tag, allowed values None, "component",
and "request".  "request" brings the original request arguments to the
component no matter where it is, "component" are the component specific args
- compiler magic number checking added, will force an error if files
compiled with an incompatible compiler version are pulled in by interpreter
- streamlining of synchronization code, container code, to be clearer,
use less memory, take less time
- docs docs docs, overhauled, corrected, beefed up, includes new paradigms
- ApacheHandler handle() method sends all **params when it first creates 
its Interpreter object

0.94c/0.94d 
- container issue with busy lock fixed 
- container test suite improved to catch aforementioned bug in the future 
- synchronizer thread strategy glitch fixed with reentrant read lock call 
- dbm container has optional flag "nonblocking", accessible via cache as 
"cache_nonblocking", which allows compatibility with gdbm, or any dbm that 
cant have multiple writers opening the file 
- dbm container also has "dbmmodule" parameter which is a pointer to the dbm 
that should be used, defaults to 'anydbm' 
- request has experimental call_content() method added, allows you to call an
embedded content function without it being buffered (you do the push_buffer
yourself)

0.94b
- bug fixes to utility classes
- small adjustments to caching code
- small documentation adjustments

0.94a
- tweaks to container to elimainte circular references, fixes high-volume 
operation
- refactoring of syncdict
- bug fixes to synchronizer
- caching properly sets file based caching as default when data_dir is present
- cache_self can optionally cache for a component other than the current

0.94
- implemented full cache API
- implemented a file (DBM)/memory container API for usage by the cache, and 
in the future a session object
- overhauled synchronization code to allow for containment API's 
more complex usage patterns
- small bug fixes with Lexer, exception reporting

0.93e
- fixed major bugs with windows platforms regarding path arithmetic and 
component loading
- bug fixes in new exception reporting, tracking of source lines. added
HTML formatting of exception reporting, error lines shown in context, all
non-myghty-internal tracelines shown in friendly stack trace
- added sha-encoded path schemes to use in creating lockfiles as well 
as cache files
 
0.93d
- fixed bug in component.py that affected components with both %requestlocal and
 %threadlocal sections
- fixed bug in lexer.py that was returning incorrect source line for syntax 
errors
- fixed bug in lexer.py that was leading to "Lexer Bug?" error messages with 
component-close tags
- added reverse-template-lookup exception reporting for file-compiled 
components !

0.93c
- %flags section can also be specified as key=value pairs within %method or 
%def tags
- 'trim' flag added with possible values 'left', 'right', 'both': trims 
whitespace around component output, implies autoflush=False
- fixed improper flock() usage that was breaking on solaris platforms
- added support for plain directory requests to resolve to dhandlers
- refactored file/thread synchronizer objects to use two separate strategies, 
one for file-only synchronization, the other for thread-only synchronization
- continued ruminating on caching architecture

0.93b
- documentation worked to be in multiple files
- synchronizer library added, applied to component compilation in interp.py to
 lock multiple forks from overwriting each other
- small bugs - interp.make_component(), subcomponent.get_dir_name() methods 
fixed
- apachehandler/cgihandler can log errors either as single line or stack traces
 to the log file, using new config name log_error, also output of errors to 
browser can be disabled via config name output_error

0.93a
- LRUCache fixed, interpreter properly caches loaded components (was more 
noticeable with very small cache sizes)
- interpreter can log expensive operations to a file with debug_file option
- the autoflush %flag in a component file is now inherited from superclass
components, if not present locally
- request maintains a cache of fetched components to greatly reduce the number 
of filesystem checks within a request, for a repeatedly referenced component, 
comopnent.get_parent_component() also uses this service to speed up method 
searches
- request object properly checks if it was called more than once

0.93
- recursive inheritance schemes are trapped and reported
- slight presentation glitch with exception reporting in ApacheHandler fixed
- component calls were not returning their return value for a dynamic 
call, which applies to any component with a %threadlocal or %requestlocal 
section, so thats fixed (requires that you recompile those pages)
- added "dynamic template" example that is also illustrated on the site
- ApacheHandler uses _content_type_set request member to determine whether or 
not to set default content type of text/html

0.92a
- buffering methodology in request simplified, sped up handling for 
autoflush->enabled
- autoflush = True|False flag added to %flags section for all components, will 
override parent component's/request's autoflush settings.  Particularly 
useful for filter components.
- out_buffer argument can be sent to ApacheHandler.handle() for optional 
capturing of the top-level apache request
- documentation editing

0.92
- %global section can call fetch_component from request
instance
- %threadlocal and %requestlocal have access to m, ARGS, globals now
- m.comp() properly returns the component return value
- flags and attr work properly, were stringifying everything before

0.91b
- tweaks to util, etc. to handle Python 2.2
- documentation page mostly completed

0.91a

- added send_redirect to request, both hard and soft redirects
- more documentation (still not done...)
- added examples section
- CGIHandler and ApacheHandler have greater control over headers
- improved subrequests behavior and error handling


0.91
initial release
