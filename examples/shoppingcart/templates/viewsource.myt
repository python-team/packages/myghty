<%args>
	name
	source
</%args>

<link rel="stylesheet" href="<& common.myc:document_uri &>/syntaxhighlight.css"></link>

<span class="headertext2">Source of <% name %></span>

<pre style="border:1px solid;padding:10px;">
<% source %>
</pre>
